﻿using UnityEngine;
using System.Collections;
using System;
using Assets.Resources.Code.Scripts;

/// <summary>
/// Clock timer class
/// </summary>
/// <remarks>class associated to the clock timer object</remarks>
public class ClockScript : MonoBehaviour 
{
	/// <summary>
	/// reference for the arrow in the timer
	/// </summary>
	private Transform arrow;

    /// <summary>
    /// store the initial angle of the arrow
    /// </summary>
    private Quaternion initialRotation;
	
    /// <summary>
    /// create reference to the clock timer object before the start
    /// </summary>
	void Awake() 
	{
        // find the arrow object and assign it
		arrow = transform.FindChild("arrow_min");
        // if cannot fond the object give an arroe
		if (arrow == null) Debug.LogError("Missing 'arrow_min' object");    
        // init arrows rotation
		InitArrows();
	}
	
    /// <summary>
    /// update the arrow rotation value
    /// </summary>
    /// <param name="deltaAng_tmp"></param>
	public void UpdateClock ( float deltaAng_tmp ) 
	{
        // rotate arrow
        arrow.transform.Rotate(0, deltaAng_tmp, 0);
	}
	
    /// <summary>
    /// set the rotation of the arrow to the initial value and store this value
    /// </summary>
	void InitArrows()
	{
		// rotate arrows
		arrow.transform.Rotate(0, 0, 0);
        initialRotation = arrow.rotation;
	}

    /// <summary>
    /// reset the arrow rotation angle to the initial value
    /// </summary>
    public void restArrow()
    {
        // rest the arrow rotation initial value
        arrow.transform.rotation = initialRotation;
        arrow.transform.Rotate(90, 0, 0);
        arrow.transform.Rotate(0, 180, 0);
    }
}
